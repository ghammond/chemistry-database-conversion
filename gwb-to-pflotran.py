import sys
import os

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math
import re
from common import *

if len(sys.argv) < 2:
    print('\n A chemistry database filename must be entered on the '
          'command line. E.g.\n\n'
          '    python %s <filename>\n'%sys.argv[0])
    sys.exit(0)

filename = sys.argv[1]
if not os.path.isfile(filename):
    print('File "%s" does not exist within directory "%s".'
          %(filename,os.getcwd()))
    sys.exit(0)

AQUEOUS = 1
MINERAL = 2
GAS = 3
OXIDE = 4

def BlankLine(line):
    if len(line.strip()) < 1:
        return True
    else:
        return False
    
def ReadChargeSizeWeight(line):
    w = line.strip().split()
    return float(w[1]), float(w[4]), float(w[8])

def ReadVolumeWeight(line):
    w = line.strip().split()
    return float(w[2]), float(w[6])

def ReadPairs(line):
    pairs = []
    w = line.split()
    for i in range(0,len(w),2):
        pairs.append((float(w[i]),w[i+1]))
    return pairs
    
def ReadElements(f,name,line):
    if len(line.strip()) == 0:
        line = f.readline()
    w = line.strip().split()
    num_elements = int(w[0]) 
    elements = []
    while True:
        line = f.readline()
        if BlankLine(line):
            break
        pairs = ReadPairs(line)
        for pair in pairs:
            elements.append(pair)
        if num_elements == len(elements):
            break
    if not num_elements == len(elements):
        print_err_msg('Mismatch in element read for {}: {} {}'.
                      format(name,num_elements,len(elements)))
        
def ReadLogKs(f,name):
    logKs = []
    while True:
        line = f.readline()
        if BlankLine(line):
            break
        w = line.strip().split()
        for word in w:
            logKs.append(float(word))
    return logKs

class Aqueous():
    def __init__(self,f,name):
        print(name)
        self.name = name
        self.charge, self.size, self.weight = ReadChargeSizeWeight(f.readline())
        self.elements = ReadElements(f,name,'')
        self.logKs = ReadLogKs(f,name)

class Mineral():
    def __init__(self,f,name):
        print(name)
        self.name = name
        f.readline() # skip formula line
        self.molar_volume, self.weight = ReadVolumeWeight(f.readline())
        self.elements = ReadElements(f,name,'')
        self.logKs = ReadLogKs(f,name)
        
class Gas():
    def __init__(self,f,name):
        print(name)
        self.name = name
        self.weight = float(f.readline().split()[2])
        while True:
            line = f.readline()
            if not line.strip().startswith(('chi','Pcrit')):
                break
            print(line)
        self.elements = ReadElements(f,name,line)
        self.logKs = ReadLogKs(f,name)
        
class Oxide():
    def __init__(self,f,name):
        print(name)
        self.name = name
        self.weight = float(f.readline().split()[2])
        self.elements = ReadElements(f,name,'')
       
def ReadSpecies(f,itype):
    species = {}
    icount = 0
    while True:
         line = f.readline()
         line = line.strip()
         if line.startswith('-end-'):
             break
         if BlankLine(line):
             continue
         if itype == AQUEOUS:
             species[line] = Aqueous(f,line)
         elif itype == MINERAL:
             name = line.split()[0]
             species[name] = Mineral(f,name)
         elif itype == GAS:
             icount += 1
             species[line] = Gas(f,line)
         elif itype == OXIDE:
             species[line] = Oxide(f,line)                
    return species
        

f = open(filename,'r')

gwb_dictionary = {}

while True:
    line = f.readline().strip()
    print(line)
    if line.startswith('*'):
        continue
    if BlankLine(line):
        continue
    if line.endswith('basis species'):
        gwb_dictionary['basis species'] = ReadSpecies(f,AQUEOUS)
    if line.endswith('redox couples'):
        gwb_dictionary['redox species'] = ReadSpecies(f,AQUEOUS)
    if line.endswith('aqueous species'):
        gwb_dictionary['secondary species'] = ReadSpecies(f,AQUEOUS)
    if line.endswith('minerals'):
        gwb_dictionary['minerals'] = ReadSpecies(f,MINERAL)
    if line.endswith('gases'):
        gwb_dictionary['gases'] = ReadSpecies(f,GAS)
    if line.endswith('oxides'):
        gwb_dictionary['oxides'] = ReadSpecies(f,OXIDE)
        break
    
f.close()